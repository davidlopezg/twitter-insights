import {TwitterHelper} from './twitter_helper';
import {TwitterParams} from './Models/params';
import * as payloadHelper from './payload_helper';

/**
* Main function for lambda instace
* @param {TwitterParams} event Evento
* @return {Promise} Retorna una promesa
*/
export function handler(event: TwitterParams) {
  console.log('Init Lambda execution!!!');
  const twitterHelper: TwitterHelper = new TwitterHelper();
  const config = JSON.parse(event.config);

  const eventConf : any = {
    ds_name: event.ds_name,
    current_step: 'init',
    config: config,
    run_id: event.run_id,
    data: '',
  };

  return new Promise((resolve, reject) => {
    if (event.path === 'getStatisticsByDate') {
      twitterHelper.getStatisticsByDate(event)
        .then((result) => {
          console.log('getStatisticsByDate OK : ', JSON.stringify(result));

          eventConf.data = JSON.stringify([{report: result}]);
          payloadHelper.sendPayload(eventConf, function(error) {
            if (error) reject(error);
            else resolve(result);
          });
        })
        .catch((error) => {
          console.log('getStatisticsByDate ERROR : ', JSON.stringify(error));
          reject(error);
        });
    } else {
      twitterHelper.processEvent(event)
        .then((result) => {
          console.log('processEvent OK : ', JSON.stringify(result));

          eventConf.data = JSON.stringify([{report: result}]);
          payloadHelper.sendPayload(eventConf, function(error) {
            if (error) reject(error);
            else resolve(result);
          });
        })
        .catch((error) => {
          console.log('processEvent ERROR : ', JSON.stringify(error));
          reject(error);
        });
    }
  });
};
