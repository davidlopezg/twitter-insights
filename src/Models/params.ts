import * as Twitter from 'twitter'

export enum methodType {
    Get = "GET",
    Post = "POST"
}

export interface TwitterParams {
    start_time? : number,
    end_time? : number,
    credentials : Twitter.AccessTokenOptions | Twitter.BearerTokenOptions,
    method : methodType,
    path? : string,
    params? : Twitter.RequestParams,
    ds_name? : string,
    config? : any,
    run_id? : string
}