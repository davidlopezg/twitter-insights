import * as Twitter from 'twitter';
import * as async from 'async';
import {TwitterParams, methodType} from './Models/params';

export class TwitterHelper {
  /**
   * Constructor
   */
  constructor() { }

  /**
   * Method used for call the Twitter API
   * @param {TwitterParams} event Required parameters to process the event
   * @return {Promise<Twitter.ResponseData>} Returns a promise
   */
  processEvent(event: TwitterParams): Promise<Twitter.ResponseData> {
    return new Promise((resolve, reject) => {
      try {
        // console.log('Porcess event : ', event);
        const credentials = event.credentials;
        const path = event.path || '';
        const params = event.params;

        const twitterClient: Twitter = new Twitter(credentials);

        switch (event.method) {
          case methodType.Get:
            twitterClient.get(path, params)
              .then((result) => resolve(result))
              .catch((error) => reject(error));
            break;


          case methodType.Post:
            twitterClient.post(path, params)
              .then((result) => resolve(result))
              .catch((error) => reject(error));
            break;


          default:
            reject('Error porque no existe el metodo');
            break;
        }
      } catch (error) {
        console.log('TwitterHelper:processEvent [ERROR] :', error);
        reject(error);
      }
    });
  };

  /**
   * Method used for get the statistics for a user in a date range
   *
   * @param {TwitterParams} event Required parameters to process the event
   * @return {Promise<Twitter.ResponseData>} Returns a promise
   */
  getStatisticsByDate(event: TwitterParams): Promise<Twitter.ResponseData> {
    return new Promise((resolve, reject) => {
      try {
        const credentials = event.credentials;
        const path = 'statuses/user_timeline';
        const params = event.params || {};

        const twitterClient: Twitter = new Twitter(credentials);

        let results: any[] = [];
        let lastId: number = 0;
        let searchInProgress: boolean = true;
        const startTime: number = event.start_time || 0;
        const endTime: number = event.end_time || (new Date().getTime());

        async.whilst(
          (): boolean => {
            return searchInProgress;
          },
          (cb) => {
            twitterClient.get(path, params)
              .then((result: Twitter.ResponseData) => {
                const index = result.findIndex((x: any) => x.id === lastId);
                if (index > -1)
                  result.splice(index, 1);

                if (result.length === 0) {
                  searchInProgress = false;
                } else {
                  results = results.concat(result.filter(
                    (x: any) =>
                      (new Date(x.created_at).getTime()) >= startTime &&
                      (new Date(x.created_at).getTime()) <= endTime));

                  lastId = result[result.length - 1].id;
                  params.max_id = lastId;
                }

                if (result.find((x: any) => (new Date(x.created_at).getTime()) < startTime))
                  searchInProgress = false;

                cb(null);
              })
              .catch((error) => {
                cb(error);
              });
          },
          (error) => {
            if (error) reject(error);
            else resolve(results);
          });
      } catch (error) {
        console.log('TwitterHelper:getStatisticsByDate [ERROR] :', error);
        reject(error);
      }
    });
  }
}