// IMPORTS
import * as lambda from '../index';
import {TwitterParams} from '../Models/params';

//const datasource = require('../../assets/datasource.json');
const datasource = require('../../assets/datasource_datefilter.json');
const event: TwitterParams = datasource;

lambda.handler(event)
  .then(() => console.log('done'))
  .catch((error) => console.log('error', JSON.stringify(error)));


